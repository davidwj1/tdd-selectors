import selectorFactory from "./selectFactory";

describe("selectorFactory", () => {
  test("accepts input selectors and a result function", () => {
    const inputSelector1 = (state) => state.input1;
    const inputSelector2 = (state) => state.input2;
    const resultFunction = (input1, input2) => input1 + input2;

    const selector = selectorFactory(
      inputSelector1,
      inputSelector2,
      resultFunction
    );
    expect(selector).toBeInstanceOf(Function);
  });

  test("selector returns the correct result", () => {
    const inputSelector1 = (state) => state.input1;
    const inputSelector2 = (state) => state.input2;
    const resultFunction = (input1, input2) => input1 + input2;

    const selector = selectorFactory(
      inputSelector1,
      inputSelector2,
      resultFunction
    );
    const state = { input1: 2, input2: 3 };

    expect(selector(state)).toEqual(5);
  });

  test("selector memoizes the result function", () => {
    const inputSelector1 = (state) => state.input1;
    const inputSelector2 = (state) => state.input2;
    const resultFunction = jest.fn((input1, input2) => input1 + input2);

    const selector = selectorFactory(
      inputSelector1,
      inputSelector2,
      resultFunction
    );
    const state = { input1: 2, input2: 3 };

    selector(state);
    selector(state);

    expect(resultFunction).toHaveBeenCalledTimes(1);
  });

  test("memoization works with multiple selectors created using the same selectorFactory", () => {
    const inputSelector1 = (state) => state.input1;
    const inputSelector2 = (state) => state.input2;
    const resultFunction = jest.fn((input1, input2) => input1 + input2);

    const selector1 = selectorFactory(
      inputSelector1,
      inputSelector2,
      resultFunction
    );
    const selector2 = selectorFactory(
      inputSelector1,
      inputSelector2,
      resultFunction
    );

    const state1 = { input1: 2, input2: 3 };
    const state2 = { input1: 4, input2: 5 };

    selector1(state1);
    selector1(state1);
    selector2(state2);
    selector2(state2);

    expect(resultFunction).toHaveBeenCalledTimes(2);
  });

  test("selector recalculates the result when the input values change", () => {
    const inputSelector1 = (state) => state.input1;
    const inputSelector2 = (state) => state.input2;
    const resultFunction = jest.fn((input1, input2) => input1 + input2);

    const selector = selectorFactory(
      inputSelector1,
      inputSelector2,
      resultFunction
    );
    const state1 = { input1: 2, input2: 3 };
    const state2 = { input1: 4, input2: 5 };

    selector(state1);
    selector(state2);

    expect(resultFunction).toHaveBeenCalledTimes(2);
  });

  test("selectors can be composed together", () => {
    const inputSelector1 = (state) => state.input1;
    const inputSelector2 = (state) => state.input2;
    const composedResultFunction = (input1, input2) => input1 * input2;

    const composedSelector = selectorFactory(
      inputSelector1,
      inputSelector2,
      composedResultFunction
    );

    const inputSelector3 = (state) => state.input3;
    const finalResultFunction = (composedResult, input3) =>
      composedResult - input3;

    const finalSelector = selectorFactory(
      composedSelector,
      inputSelector3,
      finalResultFunction
    );

    const state = { input1: 2, input2: 3, input3: 4 };

    expect(finalSelector(state)).toEqual(2);
  });

  test("memoization works with composed selectors", () => {
    const inputSelector1 = (state) => state.input1;
    const inputSelector2 = (state) => state.input2;
    const composedResultFunction = jest.fn((input1, input2) => input1 * input2);

    const composedSelector = selectorFactory(
      inputSelector1,
      inputSelector2,
      composedResultFunction
    );

    const inputSelector3 = (state) => state.input3;
    const finalResultFunction = jest.fn(
      (composedResult, input3) => composedResult - input3
    );

    const finalSelector = selectorFactory(
      composedSelector,
      inputSelector3,
      finalResultFunction
    );

    const state = { input1: 2, input2: 3, input3: 4 };

    finalSelector(state);
    finalSelector(state);

    expect(composedResultFunction).toHaveBeenCalledTimes(1);
    expect(finalResultFunction).toHaveBeenCalledTimes(1);
  });
});
